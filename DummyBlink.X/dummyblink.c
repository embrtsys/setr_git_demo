/* 
 * File:   dummyblink.c
 * Author: Pedro Fonseca
 *
 * Dummy program to test downloading programs to Digilent's Max32 board. 
 * 
 * Causes LD4 to blink. 
 * 
 * Created on 31 de Janeiro de 2019, 17:33
 * 
 */

#include <xc.h>
#include <stdint.h>

int main(int argc, char** argv) {

    uint32_t counter;

    TRISAbits.TRISA3 = 0; /* Bit RA3 is output */

    /* Start with LED ON */

    PORTAbits.RA3 = 1;

    while (1) {
        for (counter = 0; counter < 10000; counter++) {
            asm("nop");
        }
        PORTAINV = 0x0F;

    }

    return (EXIT_SUCCESS);
}

